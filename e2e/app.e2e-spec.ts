import { Cardboard2Page } from './app.po';

describe('cardboard2 App', () => {
  let page: Cardboard2Page;

  beforeEach(() => {
    page = new Cardboard2Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
