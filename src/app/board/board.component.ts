import { Component, OnInit, Input } from '@angular/core';
import { Deck } from '../deck.model';

// Services
import WinService from '../win.service';
import DeckService  from '../deck.service';
import ClicksService from '../clicks.service';
import CalculateGameService from '../calculate-game.service';

@Component({
  selector: 'board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent {
	
	@Input() level:string;
  pairs = 0;
  twoCards = [];

  deck = DeckService.getDeck();

	constructor() {	}


	ngOnInit() {
	}

  flipCard(card){
    if(card.isShown == true){
      return;
    }
  
    if(this.twoCards.length < 2) {
      this.twoCards.push(card);
      card.isShown = !card.isShown;
      ClicksService.incrementClicks();
    
      if(this.twoCards.length == 2){
        this.checkTwo();
      }
      else {
        return;
      }
    }
    
  }

  checkTwo(){
    if(this.twoCards.length !== 2){
      return;
    }

    else if(this.twoCards.length == 2) {
      if(this.twoCards[0].type == this.twoCards[1].type) {
        this.pairs = this.pairs + 1;
        
        if(this.pairs == DeckService.getTotalPairs()){
          console.log('You Won !!!');
          CalculateGameService.analyzeGame();
          WinService.setWon();
          
          return;
        }
        this.twoCards.length = 0;
        return;
      }
      else {        
        setTimeout(()=> {
          this.twoCards[0].isShown = false;
          this.twoCards[1].isShown = false;
          this.twoCards.length = 0;        
        }, 1000); 
      }
    }

  }

	
}
