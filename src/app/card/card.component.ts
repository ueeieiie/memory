import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() isShown; 
  @Input() imgUrl;
  @Output() onCardClick = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
