import { Component, OnInit, Output, EventEmitter } from '@angular/core';

// Services
import DeckService  from '../deck.service';

@Component({
  selector: 'registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  @Output() onRegistered = new EventEmitter();
	levels: string[] = ['easy', 'medium', 'hard'];
  
  constructor() { }

  ngOnInit() {
  }

}
