import { Injectable } from '@angular/core';

@Injectable()
class WinService {
  won = false;
 
  constructor() { }

  setWon(){
    this.won = true;
  }
  didWin(){
    return this.won
  }
}

export default new WinService();
