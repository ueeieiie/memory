import { TestBed, inject } from '@angular/core/testing';

import { ClicksService } from './clicks.service';

describe('ClicksService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClicksService]
    });
  });

  it('should be created', inject([ClicksService], (service: ClicksService) => {
    expect(service).toBeTruthy();
  }));
});
