import { Component, OnInit } from '@angular/core';

// Services
import ClicksService from '../clicks.service';
import CalculateGameService from '../calculate-game.service';

@Component({
  selector: 'win-msg',
  templateUrl: './win-msg.component.html',
  styleUrls: ['./win-msg.component.scss']
})
export class WinMsgComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  getTotalClicks(){
    return ClicksService.getTotalClicks();
  }

  getMessage(){
    return CalculateGameService.getMessage();
  }

}
