import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WinMsgComponent } from './win-msg.component';

describe('WinMsgComponent', () => {
  let component: WinMsgComponent;
  let fixture: ComponentFixture<WinMsgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WinMsgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WinMsgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
