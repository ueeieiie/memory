import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RegistrationComponent } from './registration/registration.component';
import { BoardComponent } from './board/board.component';
import { CardComponent } from './card/card.component';
import { ScoreboardComponent } from './scoreboard/scoreboard.component';
import { WinMsgComponent } from './win-msg/win-msg.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    BoardComponent,
    CardComponent,
    ScoreboardComponent,
    WinMsgComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
