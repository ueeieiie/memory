import { TestBed, inject } from '@angular/core/testing';

import { CalculateGameService } from './calculate-game.service';

describe('CalculateGameService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CalculateGameService]
    });
  });

  it('should be created', inject([CalculateGameService], (service: CalculateGameService) => {
    expect(service).toBeTruthy();
  }));
});
