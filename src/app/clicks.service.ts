import { Injectable } from '@angular/core';

@Injectable()
class ClicksService {
  public clickCounter: number = 0;

  constructor() { }

  public incrementClicks(){
    this.clickCounter = this.clickCounter + 1;
  }

  public getTotalClicks(){
    return this.clickCounter;
  }

}

export default new ClicksService();
