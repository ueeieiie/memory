import { Component, OnInit } from '@angular/core';

//Services
import ClicksService from '../clicks.service';

@Component({
  selector: 'scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.scss']
})
export class ScoreboardComponent implements OnInit {
  
  constructor() { }

  ngOnInit() {
  }

  getTotalClicks(){
    return ClicksService.getTotalClicks();
  }

}
