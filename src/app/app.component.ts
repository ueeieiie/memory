import { Component } from '@angular/core';

//Services
import DeckService from './deck.service';
import WinService from './win.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
	isRegistered: boolean = false;
	deck = null;

	register(event){
		this.isRegistered = !this.isRegistered;
		
		switch(event.level){
			case 'easy':
				DeckService.createDeck(2);
				DeckService.getDeck();
				break;

			case 'medium':
				DeckService.createDeck(8);
				DeckService.getDeck();				
				break;

			case 'hard':
				DeckService.createDeck(18);
				DeckService.getDeck();				
				break;

			default:
				console.log('error with level');
		}
	}

	didWin(){
		return WinService.didWin();
	}
}
