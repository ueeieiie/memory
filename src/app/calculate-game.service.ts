import { Injectable } from '@angular/core';

// Services
import ClicksService from './clicks.service';
import DeckService from './deck.service';

@Injectable()
class CalculateGameService {
  
  messages = {
    excellent: 'Im not sure if its your memory or just luck, anyway you are a genius!!!',
    ok: 'You played ok, but you I believe you can do better',
    bad: 'Memory is not your thing, try a different game'
  };

  message = this.messages.ok;

  constructor() { }
 
 // Messaging System
  analyzeGame(){
    
    const totalClicks = this.getTotalClicks();
    const totalPairs = DeckService.getTotalPairs(); 

    switch(totalPairs){
      case 2:      
        if(totalClicks >= 4 && totalClicks <= 6){
          this.setMessage(this.messages.excellent);
        }
        else if(totalClicks > 6 && totalClicks <= 8) {
          this.setMessage(this.messages.ok);
        }
        else {
          this.setMessage(this.messages.bad); 
        }
        break;

      case 8:
        if(totalClicks >= 8 && totalClicks <= 22){
          this.setMessage(this.messages.excellent);
        }
        else if(totalClicks > 22 && totalClicks <= 34) {
          this.setMessage(this.messages.ok);
        }
        else {
          this.setMessage(this.messages.bad); 
        }
        break;

      case 18:
        if(totalClicks >= 18 && totalClicks <= 36){
          this.setMessage(this.messages.excellent);
        }
        else if(totalClicks > 36 && totalClicks <= 54) {
          this.setMessage(this.messages.ok);
        }
        else {
          this.setMessage(this.messages.bad);
        }

      default:
        console.log('error');
        break;
    }
  }

  setMessage(message){
    this.message = message
  }

  getMessage(){
    return this.message;
  }


// ClicksService Methods
  getTotalClicks(){
    return ClicksService.getTotalClicks();
  }
}

export default new CalculateGameService();
