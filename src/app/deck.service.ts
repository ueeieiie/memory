import { Injectable } from '@angular/core';
import { Deck } from './deck.model';

@Injectable()
class DeckService {
  
  // temporals
  totalDeck;
  deckCopy1;
  deckCopy2;

  myDeck;
  totalPairs: number;

  constructor() { }


  public createDeck(amount){
    
    this.setTotalPairs(amount);

    // Create 2 instances of Deck
    this.deckCopy1 = new Deck().deck;
    this.deckCopy2 = new Deck().deck;
    
    // Cutting the deck to the amount of Cards mentioned
    this.deckCopy1.length = amount;
    this.deckCopy2.length = amount;

    // Create 1 deck from the clones
    this.totalDeck = this.deckCopy1.concat(this.deckCopy2);

    // Shuffle Deck
    this.myDeck = this.shuffleDeck(this.totalDeck);
    return this.myDeck;
  }

  setTotalPairs(amount){
    this.totalPairs = amount;
  }

  getTotalPairs(){
    return this.totalPairs;
  }

  shuffleDeck(deck) {
		for (var i = deck.length - 1; i > 0; i--) {
			var j = Math.floor(Math.random() * (i + 1));
			var temp = deck[i];
			deck[i] = deck[j];
			deck[j] = temp;
		}
		return deck;
	}

  getDeck(){
    return this.myDeck;
  }
}

export default new DeckService()
